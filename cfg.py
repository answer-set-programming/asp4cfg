import subprocess
from pipetools import where
import argparse
import time
import sys
import re

class Parser(object):
    """A parser class for CNF grammars"""

    def __init__(self, productions):
        self.__prods = productions
        self.__cache = dict()

    def __parse(self, w, var):
        """Unger's parsing method
        Input: a word, a non-terminal (an integer)
        Output: true or false"""
        if (w, var) in self.__cache:
            return self.__cache[w, var]
        else:
            n = len(w)
            if n == 1: return (var, w) in self.__prods
            for p in self.__prods:
                if p[0] == var:
                    if len(p) == 3:
                        for i in range(1, n):
                            if self.__parse(w[:i], p[1]) \
                                and self.__parse(w[i:], p[2]):
                                self.__cache[w, var] = True
                                return True
            self.__cache[w, var] = False
            return False

    def accepts(self, word):
        """Membership query
        Input: a string
        Output: true or false"""
        self.__cache.clear()
        return self.__parse(word, 0)

    def grammar(self):
        return self.__prods

def readWords(file_name):
    """From Abbadingo competition format
    """
    positives = set()
    negatives = set()
    with open(file_name, encoding='utf-8') as file:
        it = iter(file)
        line = next(it)
        number_of_words = int(line.split()[0])
        for _ in range(number_of_words):
            line = next(it)
            cls, length, *symbols = line.split()
            s = "".join(symbols)
            if cls == '1':
                positives.add(s)
            else:
                negatives.add(s)
    assert len(positives & negatives) == 0
    return positives, negatives

def all_proper_prefixes(S):
    result = set()
    for s in S:
        n = len(s)
        if n > 0:
            result.update(s[:i] for i in range(1, n + 1))
    return result

def all_proper_sufixes(S):
    result = set()
    for s in S:
        n = len(s)
        if n > 0:
            result.update(s[i:] for i in range(0, n))
    return result

def encode(X, Y, F, alphabet):
    lines = []
    for a in alphabet:
        lines.append(f"terminal({a}).")
    for s in X:
        lines.append(f"positive({s}).")
    for s in Y:
        lines.append(f"negative({s}).")
    for s in F:
        lines.append(f"factor({s}).")
    for f in F:
        n = len(f)
        if n >= 2:
            for pivot in range(1, n):
                b, c = f[:pivot], f[pivot:]
                lines.append(f"compose({f}, {b}, {c}).")
    return "\n".join(lines)

def extract(line):
    prods = set()
    for y_nums in map(str, re.findall("(?<=ypred\()[0123456789,]+", line)):
        prods.add(tuple(map(int, y_nums.split(','))))
    for pair in map(str, re.findall("(?<=zpred)\(\d+,.\)", line)):
        pair = tuple(pair[1:-1].split(','))
        prods.add( (int(pair[0]), pair[1]) )
    return prods

def synthesize(X, Y, K):
    F = all_proper_prefixes(all_proper_sufixes(X | Y))
    alphabet = set(c for s in X | Y for c in s)
    asp_facts = encode(X, Y, F, alphabet)
    with open("facts.lp", "w") as text_file:
        print(asp_facts, file=text_file)

    process = subprocess.run(['clingo', '-c', f'kconst={K-1}', 'cfg.lp'], stdout=subprocess.PIPE)
    text = process.stdout
    text = text.decode()
    prods = None
    for line in text.splitlines(False) > where(r'^(zpred|ypred)'):
        prods = extract(line)
        break
    return prods

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', help='an input file in Abbadingo format', type=str, default="train.txt")
    args = parser.parse_args()
    print('Working with file: {fileName}'.format_map({ 'fileName': args.file }))
    X, Y = readWords(args.file)

    if X & Y:
        print("Common words:", X & Y)
        sys.exit()

    whole_time = time.time()
    NVARS = 2
    while True:
        print("{}".format(NVARS))
        g = synthesize(X, Y, NVARS)
        if g:
            break
        NVARS += 1
    print(g)
    pg = Parser(g)
    print("Errors for positives:")
    for x in X:
        if not pg.accepts(x):
            print(x if x != '' else "epsilon", end = ' ')
    print("\nErrors for negatives:")
    for y in Y:
        if pg.accepts(y):
            print(y if y != '' else "epsilon", end = ' ')
    print('')
    print("Total time = %.2f s" % (time.time() - whole_time))
