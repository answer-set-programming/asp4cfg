# ASP4CFG

The tool generates a Context-Free Grammar with the minimum number of variables that is consistent with examples and counter-examples from a file in the Abbadingo format.

## Installation

Please install [clingo](https://github.com/potassco/clingo) and make it available in PATH.

The tool is implemented in Python 3.
All required python libraries can be installed via the pip tool.
```bash
pip3 install -r requirements.txt
```

## Usage

The tool requires one parameter, which is a filepath to a file in the Abbadingo format (all symbols have to be lowercased).
```
python3 cfg.py --file example1
```
The output grammar is shown as a set of tuples, which represent CFG rules, where the index _0_ represents the start variable _V0_.
The examplary output:
```
{(0, 1, 2), (1, 'a'), (0, 1, 0), (2, 'b')}
```
is equal to the following grammar:
```
V0 -> V1 V2 | V1 V0
V1 -> a
V2 -> b
```

## Datasets

The repository contains 33 datasets used in the article (numbered from 1 to 33). The remaining 7 datasets were generated dynamically from their definitions during running experiments.
